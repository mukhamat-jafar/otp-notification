package com.otp.notification.retrofit.otp;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class OTPNotificationResponse<T> {
    private String error_code;
    private String error_description;

    public OTPNotificationResponse(String error_code, String error_description) {
        this.error_code = error_code;
        this.error_description = error_description;
    }
}
