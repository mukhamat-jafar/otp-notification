package com.otp.notification.retrofit.otp;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface OTPNotificationApi {

//    @POST("44429dbc")
    @POST("notification")
    Call<OTPNotificationResponse> sendOTP(@Body OTPNotificationRequest request);
}
