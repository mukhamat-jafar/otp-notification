package com.otp.notification.controller;

import com.otp.notification.config.error.NotFoundException;
import com.otp.notification.dto.OTPRequest.OTPRequestDto;
import com.otp.notification.model.OTPRequest;
import com.otp.notification.retrofit.otp.OTPNotificationResponse;
import com.otp.notification.retrofit.otp.OTPNotificationService;
import com.otp.notification.service.OTPRequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpClientErrorException;

import javax.validation.constraints.Max;
import java.io.IOException;
import java.util.HashMap;

@RestController
@RequestMapping("api/v1/")
public class OTPRequestController {

    @Autowired
    private OTPNotificationService otpNotificationService;

    @Autowired
    private OTPRequestService otpRequestService;

    @PostMapping("generate-otp")
    public ResponseEntity listOTP(@RequestBody OTPRequest otpRequest, String getOTP) throws IOException {
        String otpData = otpRequestService.generateOTP(otpRequest, getOTP);
        OTPNotificationResponse data = otpNotificationService.postOTPNotification(otpRequest, otpData);
        return ResponseEntity.ok(data);
    }

    @PostMapping("verify-otp")
    public ResponseEntity listVerifyOtp(@RequestParam(name = "otp") @Max(6) String otp){
        OTPRequestDto otpData = otpRequestService.checkOtp(otp);
        HashMap<String, String> data = new HashMap<>();

        if (otp == otp){
            data.put("otp", "Match!");
        }

        if(otp == null){
            throw new NotFoundException("OTP Not Valid");
        }
        return ResponseEntity.ok(otp);
    }
}
